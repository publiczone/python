# Code pour faire la classification
# Cr�� le 13.07.2014
# Par: Santoni Rabarivelo
# ---------------------------------------------------------------------------

# Import des module
import arcpy, os, string, sys
from arcpy import env
from arcpy.sa import *


# V�rification des extensions necessaires (Check out any necessary licenses)
    #ici il nous faut l'extension : 'Spatial analyst'
arcpy.CheckOutExtension("spatial")

#Autorization d'�crasement des donn�es existantes
arcpy.overwriteOutput = 1

#Creer un folder qui va accueillir les donnees converties. Creation du folder recipient (Il sera ecrase s'il existe deja)
#(funcao acgis creat folder - Arctollbox, data management, workspace, creatfolder)
#recipient = arcpy.CreateFolder_management(arcpy.GetParameterAsText(0), "exercice1")

#declarer la geodatabase en entree (declaration de la space de travail en entree)
docTravail = arcpy.GetParameterAsText(0)
arcpy.env.workspace = docTravail


    #Code pour afficher tous les combinaisons possibles de composit band
##Band_1 = arcpy.GetParameterAsText(1)
##Band_2 = arcpy.GetParameterAsText(2)
##Band_3 = arcpy.GetParameterAsText(3)
##Band_4 = arcpy.GetParameterAsText(4)
##Band_5 = arcpy.GetParameterAsText(5)
##Band_7 = arcpy.GetParameterAsText(6)


###### Cr�ation des composit bands
    # Entrer et acquisitions des bandes rasters en input
Band_Rouge = arcpy.GetParameterAsText(1)
Band_Vert=arcpy.GetParameterAsText(2)
Band_Bleu=arcpy.GetParameterAsText(3)

try:   
    # Processing: Composit band
    composit_band = arcpy.CompositeBands_management("Band_Rouge;Band_Vert;Band_Bleu","compoband.tif")
    composit_band.save(docTravail/"compoband1")

except:
    print "Composite Bands example failed."
    print arcpy.GetMessages()

    
# Process: Filter
filtre_compo = arcpy.gp.Filter_sa("compoband.tif", "Output_raster", "LOW", "DATA")

# Process: Iso Cluster Unsupervised Classification
Number_of_classes= input("Entrer le nombre de classe")
classifNonSup = arcpy.gp.IsoClusterUnsupervisedClassification_sa(filtre_compo, Number_of_classes,20,10)
classifNonSup.save(docTravail/"classif01.tif")

#demander si l'utilisateurs veut un NDVI ou pas


# classification avec un choix de  m�thode par envoie de message a l'utilisateur
